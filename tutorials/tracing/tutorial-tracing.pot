msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Tracing
"
"POT-Creation-Date: 2016-06-01 20:59+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: tracing-f05.svg:46(format) tracing-f03.svg:46(format) tracing-f06.svg:46(format) tracing-f02.svg:46(format) tracing-f04.svg:42(format) tracing-f01.svg:46(format)
msgid "image/svg+xml"
msgstr ""

#: tracing-f05.svg:68(tspan) tracing-f03.svg:68(tspan) tracing-f06.svg:68(tspan) tracing-f02.svg:68(tspan) tracing-f04.svg:64(tspan)
#, no-wrap
msgid "Original Image"
msgstr ""

#: tracing-f05.svg:80(tspan)
#, no-wrap
msgid "Traced Image / Output Path"
msgstr ""

#: tracing-f05.svg:84(tspan)
#, no-wrap
msgid "(1,551 nodes)"
msgstr ""

#: tracing-f03.svg:80(tspan) tracing-f03.svg:96(tspan)
#, no-wrap
msgid "Edge Detected"
msgstr ""

#: tracing-f03.svg:84(tspan) tracing-f02.svg:84(tspan) tracing-f04.svg:80(tspan)
#, no-wrap
msgid "Fill, no Stroke"
msgstr ""

#: tracing-f03.svg:100(tspan) tracing-f02.svg:99(tspan) tracing-f04.svg:96(tspan)
#, no-wrap
msgid "Stroke, no Fill"
msgstr ""

#: tracing-f06.svg:80(tspan)
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr ""

#: tracing-f06.svg:84(tspan)
#, no-wrap
msgid "(384 nodes)"
msgstr ""

#: tracing-f02.svg:80(tspan) tracing-f02.svg:95(tspan)
#, no-wrap
msgid "Brightness Threshold"
msgstr ""

#: tracing-f04.svg:76(tspan) tracing-f04.svg:92(tspan)
#, no-wrap
msgid "Quantization (12 colors)"
msgstr ""

#: tracing-f01.svg:68(tspan)
#, no-wrap
msgid "Main options within the Trace dialog"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:51(None)
msgid "@@image: 'tracing-f01.svg'; md5=84a77c50eff0f7057e6610301d4ffffb"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:76(None)
msgid "@@image: 'tracing-f02.svg'; md5=dee88e327cea56b8998bfc5bc47a9bca"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:99(None)
msgid "@@image: 'tracing-f03.svg'; md5=59f05c2216f52a9f965e95c86aa60ac4"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:121(None)
msgid "@@image: 'tracing-f04.svg'; md5=b8d3e901eedee188cb88f891b71ad2e3"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:141(None)
msgid "@@image: 'tracing-f05.svg'; md5=108ce54ad9493c2d89c059e3e723fe32"
msgstr ""

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:153(None)
msgid "@@image: 'tracing-f06.svg'; md5=4616586fcb60d769fdd8c09ee1cb1780"
msgstr ""

#: tutorial-tracing.xml:4(title)
msgid "Tracing bitmaps"
msgstr ""

#: tutorial-tracing.xml:8(para)
msgid "One of the features in Inkscape is a tool for tracing a bitmap image into a &lt;path&gt; element for your SVG drawing. These short notes should help you become acquainted with how it works."
msgstr ""

#: tutorial-tracing.xml:16(para)
msgid "Currently Inkscape employs the Potrace bitmap tracing engine (<ulink url=\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) by Peter Selinger. In the future we expect to allow alternate tracing programs; for now, however, this fine tool is more than sufficient for our needs."
msgstr ""

#: tutorial-tracing.xml:23(para)
msgid "Keep in mind that the Tracer's purpose is not to reproduce an exact duplicate of the original image; nor is it intended to produce a final product. No autotracer can do that. What it does is give you a set of curves which you can use as a resource for your drawing."
msgstr ""

#: tutorial-tracing.xml:30(para)
msgid "Potrace interprets a black and white bitmap, and produces a set of curves. For Potrace, we currently have three types of input filters to convert from the raw image to something that Potrace can use."
msgstr ""

#: tutorial-tracing.xml:36(para)
msgid "Generally the more dark pixels in the intermediate bitmap, the more tracing that Potrace will perform. As the amount of tracing increases, more CPU time will be required, and the &lt;path&gt; element will become much larger. It is suggested that the user experiment with lighter intermediate images first, getting gradually darker to get the desired proportion and complexity of the output path."
msgstr ""

#: tutorial-tracing.xml:44(para)
msgid "To use the tracer, load or import an image, select it, and select the <command>Path &gt; Trace Bitmap</command> item, or <keycap>Shift+Alt+B</keycap>."
msgstr ""

#: tutorial-tracing.xml:56(para)
msgid "The user will see the three filter options available:"
msgstr ""

#: tutorial-tracing.xml:61(para)
msgid "Brightness Cutoff"
msgstr ""

#: tutorial-tracing.xml:66(para)
msgid "This merely uses the sum of the red, green and blue (or shades of gray) of a pixel as an indicator of whether it should be considered black or white. The threshold can be set from 0.0 (black) to 1.0 (white). The higher the threshold setting, the fewer the number pixels that will be considered to be “white”, and the intermediate image with become darker."
msgstr ""

#: tutorial-tracing.xml:82(para)
msgid "Edge Detection"
msgstr ""

#: tutorial-tracing.xml:87(para)
msgid "This uses the edge detection algorithm devised by J. Canny as a way of quickly finding isoclines of similar contrast. This will produce an intermediate bitmap that will look less like the original image than does the result of Brightness Threshold, but will likely provide curve information that would otherwise be ignored. The threshold setting here (0.0 – 1.0) adjusts the brightness threshold of whether a pixel adjacent to a contrast edge will be included in the output. This setting can adjust the darkness or thickness of the edge in the output."
msgstr ""

#: tutorial-tracing.xml:105(para)
msgid "Color Quantization"
msgstr ""

#: tutorial-tracing.xml:110(para)
msgid "The result of this filter will produce an intermediate image that is very different from the other two, but is very useful indeed. Instead of showing isoclines of brightness or contrast, this will find edges where colors change, even at equal brightness and contrast. The setting here, Number of Colors, decides how many output colors there would be if the intermediate bitmap were in color. It then decides black/white on whether the color has an even or odd index."
msgstr ""

#: tutorial-tracing.xml:126(para)
msgid "The user should try all three filters, and observe the different types of output for different types of input images. There will always be an image where one works better than the others."
msgstr ""

#: tutorial-tracing.xml:132(para)
msgid "After tracing, it is also suggested that the user try <command>Path &gt; Simplify</command> (<keycap>Ctrl+L</keycap>) on the output path to reduce the number of nodes. This can make the output of Potrace much easier to edit. For example, here is a typical tracing of the Old Man Playing Guitar:"
msgstr ""

#: tutorial-tracing.xml:146(para)
msgid "Note the enormous number of nodes in the path. After hitting <keycap>Ctrl+L</keycap>, this is a typical result:"
msgstr ""

#: tutorial-tracing.xml:158(para)
msgid "The representation is a bit more approximate and rough, but the drawing is much simpler and easier to edit. Keep in mind that what you want is not an exact rendering of the image, but a set of curves that you can use in your drawing."
msgstr ""

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-tracing.xml:0(None)
msgid "translator-credits"
msgstr ""

