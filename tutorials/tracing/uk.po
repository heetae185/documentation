# Translation of Inkscape's tracing tutorial to Ukrainian
#
#
# Nazarii Ritter <nazariy.ritter@gmail.com>, 2018
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2016-06-01 21:01+0200\n"
"PO-Revision-Date: 2018-05-01 12:58+0300\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Last-Translator: Nazarii Ritter <nazariy.ritter@gmail.com>, 2018\n"
"Language-Team: \n"
"X-Generator: Poedit 1.8.7.1\n"

#: tracing-f06.svg:46(format) tracing-f05.svg:46(format)
#: tracing-f04.svg:42(format) tracing-f03.svg:46(format)
#: tracing-f02.svg:46(format) tracing-f01.svg:46(format)
msgid "image/svg+xml"
msgstr "image/svg+xml"

#: tracing-f06.svg:68(tspan) tracing-f05.svg:68(tspan) tracing-f04.svg:64(tspan)
#: tracing-f03.svg:68(tspan) tracing-f02.svg:68(tspan)
#, no-wrap
msgid "Original Image"
msgstr "Початкове зображення"

#: tracing-f06.svg:80(tspan)
#, no-wrap
msgid "Traced Image / Output Path - Simplified"
msgstr "Векторизоване зображення / Вихідний контур – спрощений"

#: tracing-f06.svg:84(tspan)
#, no-wrap
msgid "(384 nodes)"
msgstr "(384 вузли)"

#: tracing-f05.svg:80(tspan)
#, no-wrap
msgid "Traced Image / Output Path"
msgstr "Векторизоване зображення / Вихідний контур"

#: tracing-f05.svg:84(tspan)
#, no-wrap
msgid "(1,551 nodes)"
msgstr "(1 551 nodes)"

#: tracing-f04.svg:76(tspan) tracing-f04.svg:92(tspan)
#, no-wrap
msgid "Quantization (12 colors)"
msgstr "Квантування (12 кольорів)"

#: tracing-f04.svg:80(tspan) tracing-f03.svg:84(tspan) tracing-f02.svg:84(tspan)
#, no-wrap
msgid "Fill, no Stroke"
msgstr "Заповнення, без штриха"

#: tracing-f04.svg:96(tspan) tracing-f03.svg:100(tspan)
#: tracing-f02.svg:99(tspan)
#, no-wrap
msgid "Stroke, no Fill"
msgstr "Штрих, без заповнення"

#: tracing-f03.svg:80(tspan) tracing-f03.svg:96(tspan)
#, no-wrap
msgid "Edge Detected"
msgstr "Визначені межі"

#: tracing-f02.svg:80(tspan) tracing-f02.svg:95(tspan)
#, no-wrap
msgid "Brightness Threshold"
msgstr "Поріг яскравості"

#: tracing-f01.svg:68(tspan)
#, no-wrap
msgid "Main options within the Trace dialog"
msgstr "Основні опції діалогового вікна «Векторизувати растр»"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:51(None)
msgid "@@image: 'tracing-f01.svg'; md5=84a77c50eff0f7057e6610301d4ffffb"
msgstr "@@image: 'tracing-f01.uk.svg'; md5=39b02de8054ea7cffb3e383f00352de1"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:76(None)
msgid "@@image: 'tracing-f02.svg'; md5=dee88e327cea56b8998bfc5bc47a9bca"
msgstr "@@image: 'tracing-f02.uk.svg'; md5=f9b800ba01376ada6b6dee43001bf215"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:99(None)
msgid "@@image: 'tracing-f03.svg'; md5=59f05c2216f52a9f965e95c86aa60ac4"
msgstr "@@image: 'tracing-f03.uk.svg'; md5=f663b2e1edf5b90069f4c0f3a6535b22"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:121(None)
msgid "@@image: 'tracing-f04.svg'; md5=b8d3e901eedee188cb88f891b71ad2e3"
msgstr "@@image: 'tracing-f04.uk.svg'; md5=657ec0650b43ec7c67f192535e68cb8e"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:141(None)
msgid "@@image: 'tracing-f05.svg'; md5=108ce54ad9493c2d89c059e3e723fe32"
msgstr "@@image: 'tracing-f05.uk.svg'; md5=aa3610c365d8e48a21c573403aefdc81"

#. When image changes, this message will be marked fuzzy or untranslated for you.
#. It doesn't matter what you translate it to: it's not used at all.
#: tutorial-tracing.xml:153(None)
msgid "@@image: 'tracing-f06.svg'; md5=4616586fcb60d769fdd8c09ee1cb1780"
msgstr "@@image: 'tracing-f06.uk.svg'; md5=4e937af242688e36b290e4a116848fd1"

#: tutorial-tracing.xml:4(title)
msgid "Tracing bitmaps"
msgstr "Векторизація растрових зображень"

#: tutorial-tracing.xml:8(para)
msgid ""
"One of the features in Inkscape is a tool for tracing a bitmap image into a "
"&lt;path&gt; element for your SVG drawing. These short notes should help you "
"become acquainted with how it works."
msgstr ""
"Однією з функціональних можливостей «Inkscape» є інструмент векторизації "
"растрового зображення в елемент &lt;контуру&gt; для Вашого SVG-малюнку. Ці "
"короткі нотатки ознйомлюють з тим як це працює."

#: tutorial-tracing.xml:16(para)
msgid ""
"Currently Inkscape employs the Potrace bitmap tracing engine (<ulink url="
"\"http://potrace.sourceforge.net\">potrace.sourceforge.net</ulink>) by Peter "
"Selinger. In the future we expect to allow alternate tracing programs; for "
"now, however, this fine tool is more than sufficient for our needs."
msgstr ""
"Наразі «Inkscape» використовує рушій векторизації растрових зображень "
"«Potrace» (<ulink url=\"http://potrace.sourceforge.net\">potrace.sourceforge."
"net</ulink>) Пітера Селінджера (Peter Selinger). В майбутньому очікується "
"можливість використання допоміжних програм векторизації; наразі, однак, цього "
"дивовижного інструменту більш, ніж достатньо для наших потреб."

#: tutorial-tracing.xml:23(para)
msgid ""
"Keep in mind that the Tracer's purpose is not to reproduce an exact duplicate "
"of the original image; nor is it intended to produce a final product. No "
"autotracer can do that. What it does is give you a set of curves which you "
"can use as a resource for your drawing."
msgstr ""
"Майте на увазі, що метою «Трасувальника» не є відтворення точної копії "
"оригіналу зображення; також він не призначений для створення кінцевого "
"продукту. Жоден автотрасувальник не може зробити цього. Все, що він робить – "
"це надає набір кривих, які можна використовувати в якості джерела для своїх "
"малюнків."

#: tutorial-tracing.xml:30(para)
msgid ""
"Potrace interprets a black and white bitmap, and produces a set of curves. "
"For Potrace, we currently have three types of input filters to convert from "
"the raw image to something that Potrace can use."
msgstr ""
"«Potrace» інтрепретує чорно-біле растрове зображення і створює набір кривих. "
"Для «Potrace», наразі, є три типи вхідних фільтрів для конвертації "
"необробленого зображення у щось, що «Potrace» може використовувати."

#: tutorial-tracing.xml:36(para)
msgid ""
"Generally the more dark pixels in the intermediate bitmap, the more tracing "
"that Potrace will perform. As the amount of tracing increases, more CPU time "
"will be required, and the &lt;path&gt; element will become much larger. It is "
"suggested that the user experiment with lighter intermediate images first, "
"getting gradually darker to get the desired proportion and complexity of the "
"output path."
msgstr ""
"Загалом, що більше темних пікселів в проміжному векторному зображенні, то "
"більше трасування виконає «Potrace». Якщо кількість трасувань зростає, то "
"буде необхідно більше процесорного часу і елемент &lt;контуру&gt; буде значно "
"більшим. Рекомендується, щоб спочатку користувач експериментував з легшими "
"проміжними зображеннями, поступово роблячи їх темнішими, щоб отримати "
"потрібну пропорцію і складність вихідного контуру."

#: tutorial-tracing.xml:44(para)
msgid ""
"To use the tracer, load or import an image, select it, and select the "
"<command>Path &gt; Trace Bitmap</command> item, or <keycap>Shift+Alt+B</"
"keycap>."
msgstr ""
"Щоб застосувати трасувальник, заванажте або імпортуйте зображення, виділіть "
"його та виберіть <command>«Контур» &gt; «Векторизувати растр»</command> або "
"<keycap>Shift+Alt+B</keycap>."

#: tutorial-tracing.xml:56(para)
msgid "The user will see the three filter options available:"
msgstr "Користувачу буде доступно три опції фільтрування:"

#: tutorial-tracing.xml:61(para)
msgid "Brightness Cutoff"
msgstr "Обмеження яскравості"

#: tutorial-tracing.xml:66(para)
msgid ""
"This merely uses the sum of the red, green and blue (or shades of gray) of a "
"pixel as an indicator of whether it should be considered black or white. The "
"threshold can be set from 0.0 (black) to 1.0 (white). The higher the "
"threshold setting, the fewer the number pixels that will be considered to be "
"“white”, and the intermediate image with become darker."
msgstr ""
"Просто використовується сума червоної, зеленою та синьої (або відтінків "
"сірого) складових пікселя в якості індикатору того, чи має він вважатися "
"чорним чи білим. Порогове значення може дорівнювати від 0,0 (чорний) до 1,0 "
"(білий). Що вище порогове значення, то менше пікселів буде вважатися «білими» "
"і проміжне зображення буде темнішим."

#: tutorial-tracing.xml:82(para)
msgid "Edge Detection"
msgstr "Визначення меж"

#: tutorial-tracing.xml:87(para)
msgid ""
"This uses the edge detection algorithm devised by J. Canny as a way of "
"quickly finding isoclines of similar contrast. This will produce an "
"intermediate bitmap that will look less like the original image than does the "
"result of Brightness Threshold, but will likely provide curve information "
"that would otherwise be ignored. The threshold setting here (0.0 – 1.0) "
"adjusts the brightness threshold of whether a pixel adjacent to a contrast "
"edge will be included in the output. This setting can adjust the darkness or "
"thickness of the edge in the output."
msgstr ""
"Використовується алгоритм визначення меж, розробленого Дж.Кенні (J. Canny), "
"як метод швидкого виявлення ізокліней з подібним контрастом. Цей алгоритм "
"створює проміжне растрове зображення, менш схоже на оригінал, ніж результат "
"«Порогу яскравості», але, ймовірно, надасть інформацію по кривих, яка інакше "
"проігнорується. Тут, значення порогу (0,0 – 1,0) підлаштовує поріг "
"яскравості, щоб включити в результат чи ні піксель, прилеглий до контрастної "
"межі. Цей параметр може коригувати темність або товщину межі результату."

#: tutorial-tracing.xml:105(para)
msgid "Color Quantization"
msgstr "Квантування кольорів"

#: tutorial-tracing.xml:110(para)
msgid ""
"The result of this filter will produce an intermediate image that is very "
"different from the other two, but is very useful indeed. Instead of showing "
"isoclines of brightness or contrast, this will find edges where colors "
"change, even at equal brightness and contrast. The setting here, Number of "
"Colors, decides how many output colors there would be if the intermediate "
"bitmap were in color. It then decides black/white on whether the color has an "
"even or odd index."
msgstr ""
"Результат цього фільтру видає проміжне зображення, яке дуже відрізняється від "
"перших двох, але, без сумнівів, є дуже корисним. Замість відображення ізоклін "
"яскравості чи контрасту, буде знайдено межі, в яких змінюються кольори навіть "
"за однакової яскравості чи контрасту. Тут, параметр, «Кількість кольорів» "
"визначає, скільки вихідних кольорів було б якби проміжне растрове зображення "
"було кольоровим. Потім, вибирається між чорним і білим в залежності від "
"індексу парності кольору."

#: tutorial-tracing.xml:126(para)
msgid ""
"The user should try all three filters, and observe the different types of "
"output for different types of input images. There will always be an image "
"where one works better than the others."
msgstr ""
"Користувач має спробувати всі ці три фільтри і поспостерігати за різними "
"типами результатів для різних типів вхідних зображень. Завжди будуть "
"зображення, для яких один фільтр підходить більше, аніж інші."

#: tutorial-tracing.xml:132(para)
msgid ""
"After tracing, it is also suggested that the user try <command>Path &gt; "
"Simplify</command> (<keycap>Ctrl+L</keycap>) on the output path to reduce the "
"number of nodes. This can make the output of Potrace much easier to edit. For "
"example, here is a typical tracing of the Old Man Playing Guitar:"
msgstr ""
"Після векторизації, вважається, що користувач використає <command>«Контур» "
"&gt; «Спростити»</command> (<keycap>Ctrl+L</keycap>)  для вихідного контуру "
"для зменшення кількості вузлів. Це може зробити результат «Potrace» значно "
"легшим для редагування. Наприклад, ось – типова векторизація «Старого "
"гітариста»:"

#: tutorial-tracing.xml:146(para)
msgid ""
"Note the enormous number of nodes in the path. After hitting <keycap>Ctrl+L</"
"keycap>, this is a typical result:"
msgstr ""
"Зверніть увагу на величезну кількість вузлів у контурі. Після натискання "
"<keycap>Ctrl+L</keycap>, ось – типовий результат:"

#: tutorial-tracing.xml:158(para)
msgid ""
"The representation is a bit more approximate and rough, but the drawing is "
"much simpler and easier to edit. Keep in mind that what you want is not an "
"exact rendering of the image, but a set of curves that you can use in your "
"drawing."
msgstr ""
"Зображення трохи більше апроксимоване та грубе, але малюнок значно простіший "
"і легший для редагування. Майте на увазі, що потрібна не точна векторизація "
"зображення, а набір кривих, які можна використати для свого малюнку."

#. Put one translator per line, in the form of NAME <EMAIL>, YEAR1, YEAR2
#: tutorial-tracing.xml:0(None)
msgid "translator-credits"
msgstr "Nazarii Ritter <nazariy.ritter@gmail.com>, 2018"
